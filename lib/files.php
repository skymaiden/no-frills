<!-- css reset -->
<link rel="stylesheet" href="assets/css/reset.css" />
<link rel="stylesheet" href="assets/css/ecran.css" />
<link rel="stylesheet" href="assets/css/print.css" media="print" />

<!-- libraries -->
<!-- INT to DEV : mettre modernizr dans le header !! -->
<script src="assets/js/libs/modernizr-2.0.6.min.js"></script>
<script src="assets/js/libs/jquery-1.7.min.js"></script>
<script src="assets/js/libs/jquery.bxSlider.min.js"></script>
<script src="assets/js/libs/jquery.infieldlabel.js"></script>

<script src="assets/js/script.js"></script>